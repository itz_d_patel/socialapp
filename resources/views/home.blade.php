@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="formGroupExampleInput">Search: </label>
                        <input type="text" id="searchUserText" class="form-control searchUserText" placeholder="Search User" style="width: 30%;">
                      </div>
                    </form>

                    <div class="row userList">
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        searchUser();
    });
    var ajaxObject = {};
    
    $(document).on('keyup', '#searchUserText', function(e) {
        var searchText = $(this).val();
        ajaxObject.searchText = searchText;
        searchUser(ajaxObject);
    });

    function searchUser(ajaxObject = {}) {
        ajaxObject._token = '{!! csrf_token() !!}';
        $.ajax({
            type: "POST",
            url: "{{ route('userSearch') }}",
            data: ajaxObject,
            cache: false,
            success: function(data)
            {
                $('.userList').html(data);
            } 
        });
    }
</script>
@endpush
