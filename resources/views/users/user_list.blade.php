@if(sizeof($users) > 0)
    @foreach($users as $user)
        <div class="col-sm-4 mb-3">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">{{ $user->first_name .' '. $user->last_name }}</h5>
              <p class="card-text"><span>Phone:</span> {{ $user->phone_no }}</p>
              <p class="card-text"><span>Email:</span> {{ $user->email }}</p>

                @if (isset($user->connections[0]) && $user->connections[0]->pivot->status == 'sent')
                  <a href="{{ route('acceptFriend', ['id' => $user->id]) }}" class="btn btn-success">Accept</a>
                  <a href="{{ route('rejectFriend', ['id' => $user->id]) }}" class="btn btn-danger">Reject</a>
                @elseif (isset($user->connections[0]) && $user->connections[0]->pivot->status == 'received')
                  <a href="#" class="btn btn-warning">Pending</a> 
                @elseif (isset($user->connections[0]) && $user->connections[0]->pivot->status == 'accepted')
                  <a href="#" class="btn btn-success">Friend</a>
                @else
                  <a href="{{ route('addFriend', ['id' => $user->id]) }}" class="btn btn-primary">Add Friend</a>
                @endif

            </div>
          </div>
        </div>
    @endforeach
@else
<div class="col-md-4 mb-3">
    <div class="alert alert-danger text-center" role="alert">
        Not found.
    </div>
</div>
@endif