@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('My Connections') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if(sizeof($connections) > 0)
                        @foreach($connections as $connection)
                            <div class="col-sm-4 mb-3">
                              <div class="card">
                                <div class="card-body">
                                  <h5 class="card-title">{{ $connection->friend->first_name .' '. $connection->friend->last_name }}</h5>
                                  <p class="card-text"><span>Phone:</span> {{ $connection->friend->phone_no }}</p>
                                  <p class="card-text"><span>Email:</span> {{ $connection->friend->email }}</p>
                                </div>
                              </div>
                            </div>
                        @endforeach
                    @else
                    <div class="col-md-4 mb-3">
                        <div class="alert alert-danger text-center" role="alert">
                            Not found.
                        </div>
                    </div>
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection