<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace'=>'api\v1', 'prefix'=>'v1'], function(){
	Route::post('login', 'UserController@login');
	Route::post('register', 'UserController@register');
});

Route::group(['namespace'=>'api\v1', 'prefix'=>'v1', 'middleware'=>['auth:api']], function(){
	Route::post('addEvent', 'EventController@addEvent');
	Route::post('removeEvent', 'EventController@removeEvent');
	Route::get('listEvent', 'EventController@listEvent');
});