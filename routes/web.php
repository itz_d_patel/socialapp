<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::group(['middleware' => 'auth'], function () {
	Route::get('/home', 'HomeController@index')->name('home');

	Route::post('userSearch', 'UserController@userSearch')->name('userSearch');
	Route::get('conections', 'UserController@conections')->name('conections');
	Route::get('addFriend/{id?}', 'UserController@addFriend')->name('addFriend');
	Route::get('acceptFriend/{id?}', 'UserController@acceptFriend')->name('acceptFriend');
	Route::get('rejectFriend/{id?}', 'UserController@rejectFriend')->name('rejectFriend');
});
