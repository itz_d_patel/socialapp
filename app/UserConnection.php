<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserConnection extends Model
{
    protected $fillable = ['user_id', 'friend_id', 'status'];

   	public function friend() {
   		return $this->belongsTo('App\User', 'friend_id')->select('users.id', 'first_name', 'last_name', 'users.phone_no', 'users.email',);
   	}
}
