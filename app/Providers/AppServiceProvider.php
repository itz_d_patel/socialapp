<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\User;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('unique_email', function($attribute, $value, $parameters) {
            $checkEmail = User::where($attribute,"$value");
            if(isset($parameters[0]) && $parameters[0] > 0){
                $checkEmail->where('id','!=',$parameters[0]);
            }
            $checkEmail = $checkEmail->count();
            if($checkEmail == 0)
                return true;
        });
    }
}
