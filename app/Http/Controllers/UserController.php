<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\UserConnection;

class UserController extends Controller
{
    public function userSearch(Request $request)
    {
        $users = User::where('id','!=',Auth::user()->id)->with('connections');

        if ($request->searchText != '') {
            $users->where('first_name', 'LIKE', '%' . $request->searchText . '%');
            $users->orWhere('last_name', 'LIKE', '%' . $request->searchText . '%');
        }

        $users = $users->get();
        // dd($users[0]->connections);
        return view('users.user_list')->with(['users' => $users]);
    }

    public function addFriend(Request $request)
    {
        $user = User::where(['id'=>$request->id, 'status'=>1])->firstOrFail();

        if($user) {
            Auth::user()->connections()->attach([$request->id => ['status'=>'sent']]);
        
            $user->connections()->attach([Auth::id() => ['status' => 'received']]);
        }

        $request->session()->flash('status', 'Friend request sent successfully.');
        return redirect()->route('home');
    }

    public function acceptFriend(Request $request)
    {
        $user = User::where(['id'=>$request->id, 'status'=>1])->firstOrFail();

        if($user) {
            UserConnection::where(['user_id' => Auth::id(), 'friend_id' => $request->id])->update(['status' => 'accepted']);
            UserConnection::where(['friend_id' => Auth::id(), 'user_id' => $request->id])->update(['status' => 'accepted']);
        }

        $request->session()->flash('status', 'Friend request accepted successfully.');
        return redirect()->route('home');
    }

    public function rejectFriend(Request $request)
    {
        $user = User::where(['id'=>$request->id, 'status'=>1])->firstOrFail();

        if($user) {
            Auth::user()->connections()->detach($request->user_id);
            $user->connections()->detach(Auth::id());
        }

        $request->session()->flash('status', 'Friend request rejected successfully.');
        return redirect()->route('home');
    }

    public function conections()
    {
        $connections = UserConnection::where(['user_id'=> Auth::id(), 'status' => 'accepted'])->whereHas('friend')->with('friend')->get();
        // dd($connections);
        return view('users.connections')->with(['connections' => $connections]);
    }
}
